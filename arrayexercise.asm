;;
;; arrayexercise.asm
;;
;; Solution to exercise in class notes.
;;
;; by Terry Sergeant
;;
%include "iomacros.asm"

		global	main
		section .data
endl:		db	10,0

		section .bss

a:		resd	50


		section .text
main:
		align_stack

		mov	edi, 1
		mov	rcx, a
loop:		cmp	edi, 50
		jg	exitloop
		mov	[rcx], edi
		inc	edi
		add	rcx, 4
		jmp	loop

exitloop:
		mov	rcx, a
		mov	edi, 1

printloop:
		cmp	edi, 50
		jg	part2
		put_i	[rcx]
		put_str	endl
		add	rcx, 4
		inc	edi
		jmp	printloop

		; we do the same two loops again, but this time we use the
		; more convenient array notation
part2:

		mov	edi, 0
		mov	ecx, 1
loop2:		cmp	edi, 50
		jge	exitloop2
		mov	[a + 4*edi], ecx
		inc	edi
		inc	ecx
		jmp	loop2

exitloop2:
		mov	edi, 0
printloop2:
		cmp	edi, 50
		jge	theend
		put_i	[a + 4*edi]
		put_str	endl
		inc	edi
		jmp	printloop2


theend:		mov     eax, 60
		xor     rdi, rdi
		syscall

