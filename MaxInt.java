//
// MAX_VALUE + 1 == MIN_VALUE !?
//
public class MaxInt
{
	public static void main(String [] args) {
		int a,b;

		a= Integer.MAX_VALUE;
		b= Integer.MIN_VALUE;
		System.out.println("Max: "+a);
		System.out.println("Min: "+b);

		/*
		a++;
		b--;

		System.out.println("Max+1: "+a);
		System.out.println("Min-1: "+b);

		a= Integer.MAX_VALUE - 10;

		for (b=0; b<20; b++) {
			System.out.println(a);
			a++;
		}
		*/
	}
}
