/**
 * Provides a function to read strings from a file.
 *
 * @author  T.Sergeant
 * @version Fall 2016
 *
 */

#include<stdio.h>
#include<stdlib.h>
#define MAXSTR 27


int readStrings(char a[][MAXSTR], char filename[])
{
	FILE * fp;
	int n;

	n= 0;
	fp= fopen(filename,"r");
  	while (!feof(fp)) {
  		fgets(a[n],MAXSTR,fp);
		n++;
	}
	fclose(fp);

	return n;
}
