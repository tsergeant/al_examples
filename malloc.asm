;;
;; malloc.asm
;;
;; Demonstrates dynamic memory allocation
;;
;; by Terry Sergeant
;; for AL
;;
;;
%include "iomacros.asm"
%include "dumpregs.asm"

		global main

		section .data
endl		db	10,0


		section .text
main:
		align_stack

		mov	rax,9		; syscall #9 is mmap
		xor	rdi,rdi		; addr = NULL
		mov	rsi,36		; # of bytes to reserve
		mov	rdx,3		; PROT_READ|PROT_WRITE
		mov	r10,0x22	; MAP_PRIVATE|MAP_ANONYMOUS
		mov	r8,-1		; fd=-1
		xor	r9,r9		; offset=0
		syscall
		dump_regs


		mov	rdi,rax
		mov	dword [rdi],100;
		put_i	[rdi]
		put_str	endl

		mov     rax, 60                 ; exit 0
		xor     rdi, rdi
		syscall
