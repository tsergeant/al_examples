;
; rflags.asm
;
; Demo the put_rflags register.
;
; @author  Terry Sergeant
; @version for Assembly
;
; There is a register (rflags) that is not a general purpose register (which
; means we cannot use it directly).  This register is useful, though, because
; many assembly language instructions adjust its value as a side-effect. By
; looking at the register we can sometimes determine results of the previous
; command. The macro "put_rflags" provided in dumpregs.asm shows the current
; contents of rflags (without modifying it).
;
; Instructions:
; 1) Open two windows. In one, view this source code; in the other, compile and
; run.
;
; 2) Notice that the first call to put_rflags displays something like this:
; FLG 00000000 00000246
; This is the contents of the 64-bit rflags register (in hexadecimal). We are
; only interested in the low-order two bytes of the register: 0246.
;
; 3) If you look at quick reference card handed out in class, it shows the
; structure of the low-order two bytes that compose the rflags register as
; follows:
;
; ---- ODIT SZ-A -P-C
;
; In the same document, to the right of this mapping you can see a description
; of what each flag stands for. So, if you take the first output of rflags in
; the sample program you get something like this: 0246 (hexadecimal). If you
; convert to binary and line it up with the above key you can see which bits are
; set and which are clear:
;
; ---- ODIT SZ-A -P-C
; 0000 0010 0100 0110
;
; That is, the I, Z, and P flags are all set and the others are all clear.
;
; After we do cmp r9, 0 (where r9 was 10 ... which does a subtract 10 - 0), we
; get 0206h which looks like this:
;
; ---- ODIT SZ-A -P-C
; 0000 0010 0000 0110

; Notice that the Z bit (set if result is zero) is clear ... because the result
; was 10.
;
; 4) For each of the outputs from the program, convert the results of the
; low-order bytes to binary and then line them up with key to see which
; bit were set and which were cleared by the previous command. The goal is
; to be able to explain why the flags are getting set/cleared.
;
%include "dumpregs.asm"

section .text
		global 		main
main:
		align_stack

		put_rflags	; first call
		mov	r9,10
		put_rflags	; second call
		cmp	r9,0	; comparing numbers first > second
		put_rflags	; third call
		cmp	r9,10   ; comparing numbers first = second
		put_rflags	; fourth call
		cmp	r9,12	; comparing numbers first < second
		put_rflags	; fifth call
		add	r9,1	; adding a number with a result > 0
		put_rflags	; sixth call
		mov	r9,-1
		add	r9,1	; adding a number with a result 0
		put_rflags	; seventh call
		mov	r9,-10
		add	r9,1	; adding a number with a result < 0
		put_rflags	; eight call


alldone: 	mov	ebx,0		; return 0
		mov	eax,1		; on
		int	80h		; exit

